import React from 'react';
import PropTypes from 'prop-types';
import TextField from "@mui/material/TextField";
import Grid from "@mui/material/Grid";
import MenuItem from "@mui/material/MenuItem";

const FormElement = ({label, name, value, onChange, required, error, autoComplete, type, select, options, multiline, rows}) => {
    let inputChildren = null;

    if (select) {
        inputChildren = options.map(option => (
            <MenuItem
                key={option._id}
                value={option._id}>
                {option.title}
            </MenuItem>
        ));
    }

    return (
        <Grid item xs={12}>
            <TextField
                select={select}
                multiline={multiline}
                rows={rows}
                type={type}
                required={required}
                autoComplete={autoComplete}
                label={label}
                name={name}
                value={value}
                onChange={onChange}
                error={Boolean(error)}
                helperText={error}
            >
                {inputChildren}
            </TextField>
        </Grid>
    );
};


export default FormElement;