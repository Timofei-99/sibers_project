import React from 'react';
import {Button, Card, CardActions, CardContent, CardMedia, Grid, Typography} from "@mui/material";
import {apiUrl} from "../../config";
import {NavLink} from "react-router-dom";

const Contact = ({name, phone, username, email, rename, image, id}) => {
    return (
        <Grid item sm={6} xs md={4} lg={4}>
            <Card sx={{maxWidth: 345}}>
                <CardMedia
                    component="img"
                    alt=""
                    height="250"
                    image={apiUrl + '/' + image}
                />
                <CardContent>
                    <Typography gutterBottom variant="h5" component="div">
                        {name}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Phone: {phone}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        Email: {email}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        UserName: {username}
                    </Typography>
                </CardContent>
                <CardActions>
                    <NavLink to={'/edit_contacts/' + id}>
                        <Button size="small">Rename</Button>
                    </NavLink>
                </CardActions>
            </Card>
        </Grid>
    );
};

export default Contact;