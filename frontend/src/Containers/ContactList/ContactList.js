import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchContactsRequest} from "../../store/actions/contactsActions";
import Contact from "../../Components/Contact/Contact";
import {Grid} from "@mui/material";

const ContactsList = () => {
    const dispatch = useDispatch();
    const contacts = useSelector(state => state.contacts.contacts);


    useEffect(() => {
        dispatch(fetchContactsRequest());
    }, [dispatch]);


    return (
        <Grid container spacing={2}>
            <Grid item container spacing={2} alignItems={"center"} justifyContent={"space-between"}>
                {contacts.map(contact => (
                    <Contact
                        key={contact._id}
                        name={contact.name}
                        phone={contact.phone}
                        email={contact.email}
                        username={contact.username}
                        image={contact.avatar}
                        id={contact._id}
                    />
                ))}
            </Grid>
        </Grid>
    );
};

export default ContactsList;