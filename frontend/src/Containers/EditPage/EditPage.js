import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchContactRequest, putContactsRequest} from "../../store/actions/contactsActions";
import {useNavigate, useParams} from "react-router-dom";
import Grid from "@mui/material/Grid";
import FormElement from "../../Components/UI/FormElement/FormElement";
import {Button, Container, Typography} from "@mui/material";

const EditPage = () => {
    const dispatch = useDispatch();
    const [contacts, setContacts] = useState({
        name: '',
        phone: '',
        username: '',
        email: ''
    });
    const contact = useSelector(state => state.contacts.contact);
    const params = useParams();
    const navigate = useNavigate();

    useEffect(() => {
        dispatch(fetchContactRequest(params.id));
    }, [dispatch]);

    useEffect(() => {
        setContacts(contact)
    }, [contact]);

    const contactsDataChanged = (event) => {
        const {name, value} = event.target;

        setContacts((prevState) => ({
            ...prevState,
            [name]: value,
        }));
    };

    const editContact = async (e) => {
        e.preventDefault();
       dispatch(putContactsRequest(contacts));
       navigate('/');
    }


    return (
        <Container component={'section'} maxWidth={'xs'}>
            <Grid container spacing={2} direction={'column'}>
                <Typography style={{textAlign: 'center', marginTop: '20px'}} variant={'h4'}>Change contact</Typography>
                <form onSubmit={editContact}>
                    <FormElement
                        label={'name'}
                        onChange={contactsDataChanged}
                        name={'name'}
                        value={contacts.name || ''}
                    />

                    <FormElement
                        label={'phone'}
                        onChange={contactsDataChanged}
                        name={'phone'}
                        value={contacts.phone || ''}
                    />

                    <FormElement
                        label={'username'}
                        onChange={contactsDataChanged}
                        name={'username'}
                        value={contacts.username || ''}
                    />

                    <FormElement
                        label={'email'}
                        onChange={contactsDataChanged}
                        name={'email'}
                        value={contacts.email || ''}
                    />
                    <Button type={'submit'} color={'primary'} variant={'contained'} style={{marginTop: '20px'}}>
                        Save
                    </Button>
                </form>
            </Grid>
        </Container>
    );
};

export default EditPage;