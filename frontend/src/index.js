import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import {Provider} from "react-redux";
import {BrowserRouter} from 'react-router-dom'
import {ToastContainer} from "react-toastify";

import 'react-toastify/dist/ReactToastify.css';
import store from "./store/configureStore";
import {ThemeProvider} from "@mui/material/styles";
import theme from "./theme";


const app = (
    <Provider store={store}>
        <BrowserRouter>
            <ThemeProvider theme={theme}>
                <ToastContainer/>
                <App/>
            </ThemeProvider>
        </BrowserRouter>
    </Provider>
);

ReactDOM.render(app, document.getElementById('root'));


