import {combineReducers} from "redux";
import createSagaMiddleware from 'redux-saga';
import {configureStore} from "@reduxjs/toolkit";
import rootSaga from "./saga";
import initialState from '../store/slices/contactsSlices';
import contactsSlice from "./slices/contactsSlices";
import {loadFromLocalStorage, saveToLocalStorage} from "./localStorage";

const rootReducer = combineReducers({
    "contacts": contactsSlice.reducer
});

const persistedState = loadFromLocalStorage();
const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware];


const store = configureStore({
    reducer: rootReducer,
    middleware,
    devTools: true,
    preloadedState: persistedState
});

sagaMiddleware.run(rootSaga);


store.subscribe(() => {
    saveToLocalStorage({
        contacts: store.getState().contacts
    });
});


export default store;