import contactsSlice from '../slices/contactsSlices';

export const {
    fetchContactsRequest,
    fetchContactsSuccess,
    fetchContactsFailure,
    putContactsRequest,
    putContactsSuccess,
    putContactsFailure,
    fetchContactRequest,
    fetchContactSuccess,
    fetchContactFailure
} = contactsSlice.actions;