import {put, takeEvery} from 'redux-saga/effects';
import axiosApi from "../../axiosApi";


import {
    fetchContactRequest,
    fetchContactsFailure,
    fetchContactsRequest,
    fetchContactsSuccess,
    fetchContactSuccess,
    putContactsFailure,
    putContactsRequest,
    putContactsSuccess
} from "../actions/contactsActions";
import {toast} from "react-toastify";


export function* fetchContacts() {
    try {
        const response = yield axiosApi.get('/contacts');
        yield put(fetchContactsSuccess(response.data));
    } catch (e) {
        yield put(fetchContactsFailure(e));
    }
}

export function* fetchContact({payload: id}) {
    try {
        const response = yield axiosApi.get(`/contacts/${id}`);
        yield put(fetchContactSuccess(response.data));
    } catch (e) {
        yield put(fetchContactsFailure(e))
    }
}

export function* putContact({payload}) {
    try {
        yield axiosApi.put(`/contacts/${payload._id}`, payload);
        yield put(putContactsSuccess());
        toast.success('Changed!')
    } catch (e) {
        yield put(putContactsFailure(e));
    }
}

const contactsSagas = [
    takeEvery(fetchContactsRequest, fetchContacts),
    takeEvery(fetchContactRequest, fetchContact),
    takeEvery(putContactsRequest, putContact),
];

export default contactsSagas;