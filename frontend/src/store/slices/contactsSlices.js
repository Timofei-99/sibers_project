import {createSlice} from "@reduxjs/toolkit";

const name = 'Contacts';

const initialState = {
    contacts: [],
    contact: {},
    loading: false,
    error: null
};

const contactsSlice = createSlice({
    name,
    initialState,
    reducers: {
        fetchContactsRequest: (state) => {
            state.loading = true;
        },
        fetchContactsSuccess: (state, {payload: contacts}) => {
            state.contacts = contacts;
            state.loading = false;
            state.error = null;
        },
        fetchContactsFailure: (state, {payload: error}) => {
            state.error = error;
        },
        fetchContactRequest: (state) => {
            state.loading = true;
        },
        fetchContactSuccess: (state, {payload: contact}) => {
            state.contact = contact;
            state.loading = false;
        },
        putContactsRequest: (state) => {
            state.loading = true
        },
        putContactsSuccess: (state) => {
            state.loading = false;
        }
    }
})

export default contactsSlice;