import {all} from 'redux-saga/effects';
import contactsSagas from "./sagas/contactsSagas";

export default function* rootSaga() {
    yield all([...contactsSagas])
}