import React from "react";
import {Route, Routes} from "react-router-dom";
import ContactsList from "./Containers/ContactList/ContactList";
import EditPage from "./Containers/EditPage/EditPage";


const App = () => {


    return (
        <Routes>
            <Route path={'/'} element={<ContactsList/>}/>
            <Route path={'/edit_contacts/:id'} element={<EditPage/>}/>
        </Routes>
    );
};

export default App;
