const express = require('express');
const Contact = require('../models/Contact');


const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const contacts = await Contact.find().sort({name: 1});
        res.send(contacts);
    } catch (e) {
        res.sendStatus(404);
    }
});

router.get('/:id', async (req, res) => {
    try {
        const contact = await Contact.findById(req.params.id);
        res.send(contact);
    } catch (e) {
        res.sendStatus(404);
    }
})

router.put('/:id', async (req, res) => {
    try {
        const contact = {
            name: req.body.name,
            phone: req.body.phone,
            username: req.body.username,
            email: req.body.email
        }

        if (contact)
            await Contact.updateOne({_id: req.params.id}, {...contact});
        return res.send('Contact was changed!');

    } catch (e) {
        res.sendStatus(403).send({message: 'Cannot find'});
    }
})

module.exports = router;