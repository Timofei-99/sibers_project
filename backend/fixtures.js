const mongoose = require('mongoose');
const config = require('./config');
const Contact = require('./models/Contact');

const run = async () => {
    await mongoose.connect(config.db.url);

    const collections = await mongoose.connection.db.listCollections().toArray();

    for (const coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }

    await Contact.create(
        {
            name: 'Terminator',
            phone: '+5555555',
            username: 'terminator228',
            email: 'Steel@mail.ru',
            avatar: 'fixtures/terminator.jpeg',
        },
        {
            name: 'Spider-man',
            phone: '+6666666666',
            username: 'Im pauk:)',
            email: 'Pautina@gmail.com',
            avatar: 'fixtures/spider-man.jpeg',
        },
        {
            name: 'Batman',
            phone: '+777777777',
            username: 'nightHero',
            email: 'NightKnight@gmail.com',
            avatar: 'fixtures/batman.jpeg',
        },
        {
            name: 'Thor',
            phone: '+8888888888',
            username: 'beardMan',
            email: 'Asgard777@gmail.com',
            avatar: 'fixtures/thor.jpeg',
        },
        {
            name: 'Tim',
            phone: '+996777021031',
            username: 'Tim99',
            email: 'tima9953@gmail.com',
            avatar: 'fixtures/me.jpeg',
        },
        // {
        //     name: 'Amanda',
        //     phone: '+9966666666',
        //     username: 'amanda',
        //     email: 'amanda@mail.ru',
        //     avatar: 'fixtures/amanda',
        // }
    );
    await mongoose.connection.close();
};

run().catch(console.error);