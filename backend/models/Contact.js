const mongoose = require('mongoose');
const {nanoid} = require('nanoid');


const ContactSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    phone: {
        type: Number,
        required: true
    },
    username: {
        type: String,
    },
    email: {
        type: String
    },
    avatar: {
        type: String
    }
});

const Contact = mongoose.model('Contact', ContactSchema);

module.exports = Contact;